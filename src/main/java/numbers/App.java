package numbers;

public class App {
    public static void main(String[] args) {
        Number[] numbers = new Number[4];
        numbers[0] = new Integer(3);
        numbers[1] = new Double(3.5);
        numbers[2] = new Long (3L);
        numbers[3] = new Float (-3.5);
        System.out.println(TestNumber.computeSum(numbers));
        System.out.println(TestNumber.computeSum("12,53,48",";"));
    }
}
