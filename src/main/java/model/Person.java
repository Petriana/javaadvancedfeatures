package model;

public class Person {
    private String id;
    private String nume;
    private String prenume;
    private int varsta;
    private String gen;
    private boolean estePensionar;

    Person(String id, String nume, String prenume, int varsta, String gen, boolean estePensionar) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.varsta = varsta;
        this.gen = gen;
        this.estePensionar = estePensionar;
    }

    public String getId() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public boolean getEstePensionar() {
        return estePensionar;
    }

    public void setEstePensionar(boolean estePensionar) {
        this.estePensionar = estePensionar;
    }
}
