package vehicle;

public class Vehicle {
    protected final int maxSpeed;

    public Vehicle (){
        this(50);
    }

   public Vehicle (int maxSpeed){
        this.maxSpeed = maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }
}
