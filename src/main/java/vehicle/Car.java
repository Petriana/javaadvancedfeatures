package vehicle;

public class Car extends Vehicle {
    private boolean convertible;

public Car (int maxSpeed, boolean convertible){
    super(maxSpeed);
    this.convertible = convertible;
}
   public int tuneCar (int maxSpeedIncrease){
    return (this.maxSpeed + maxSpeedIncrease);
   }

    public boolean isConvertible() {
        return convertible;
    }
}
